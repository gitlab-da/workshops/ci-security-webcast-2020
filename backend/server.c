#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

int main(void) {
    size_t pagesize = getpagesize();

    char * region = mmap(
        (void*) (pagesize * (1 << 20)),
        pagesize,
        PROT_READ|PROT_WRITE|PROT_EXEC,
        MAP_ANON|MAP_PRIVATE,
        0,
        0
    );

    if (region == MAP_FAILED) {
        perror("Could not mmap");
        return 1;
    }

    strcpy(region, "Hello GitLab Application Security Test!");

    printf("Contents of region: %s\n", region);

    int unmap_result = munmap(region, 1 << 10);
    if (unmap_result != 0) {
        perror("Could not munmap");
        return 1;
    }

    FILE *fp;
    fp = fopen("gitlab.keksi", "r");
    fprintf(fp, "Hello from GitLab");
    fclose(fp);
    chmod("gitlab.keksi", S_IRWXU|S_IRWXG|S_IRWXO);

    return 0;
}
