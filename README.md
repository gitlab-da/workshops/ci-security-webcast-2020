# GitLab CI Security Webcast

## From DevOps to DevSecOps: Automate your security tests with CI

- [Slides](https://docs.google.com/presentation/d/e/2PACX-1vSWUhh5kPuzUzvvI6lrv3DJz1PJlixG4oxO-zQx6unC6NkjCyB2VVoOavKy-5jODGa7QMPd3MVABzUA/pub?start=false&loop=false&delayms=3000)
